package nz.co.fakedomain.circuitbreakers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
class ServiceStatusController(val httpClient: MyCoolHttpClient) {
    @PutMapping("/kill")
    @ResponseBody
    fun killTheClient(): String {
        httpClient.breakClient()
        return """{"state": "broken"}"""
    }

    @PutMapping("/fix")
    @ResponseBody
    fun fixTheClient(): String {
        httpClient.fixClient()
        return """{"state": "fixed"}"""
    }
}
