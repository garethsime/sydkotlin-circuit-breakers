package nz.co.fakedomain.circuitbreakers

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class MyCoolHttpClient() {
    object MyCoolHttpClient {
        val log = LoggerFactory.getLogger(MyCoolHttpClient::class.java)
    }

    var state = State.OK;

    fun post(): String = when (state) {
        State.OK -> "Kotlin is a pretty cool guy"
        State.BROKEN -> throw RuntimeException("I'm broken right now")
    }

    fun fixClient() {
        MyCoolHttpClient.log.info("Fixing the client")
        state = State.OK
    }

    fun breakClient() {
        MyCoolHttpClient.log.info("Breaking the client")
        state = State.BROKEN
    }

    enum class State {
        OK, BROKEN
    }
}
