package nz.co.fakedomain.circuitbreakers

import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ServiceConfiguration {
    @Bean
    fun usefulService(
            httpClient: MyCoolHttpClient,
            circuitBreakerRegistry: CircuitBreakerRegistry
    ): UsefulService = ServiceA(
            httpClient,
            circuitBreakerRegistry.circuitBreaker("things")
    )
}