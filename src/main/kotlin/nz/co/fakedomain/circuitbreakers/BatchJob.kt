package nz.co.fakedomain.circuitbreakers

import org.slf4j.LoggerFactory
import org.springframework.context.SmartLifecycle
import org.springframework.stereotype.Service
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

@Service
class BatchJob(val usefulService: UsefulService): SmartLifecycle {

    companion object {
        val log = LoggerFactory.getLogger(BatchJob::class.java)
    }

    var scheduled: ScheduledExecutorService? = null

    override fun isAutoStartup(): Boolean = true
    override fun getPhase(): Int = 0
    override fun isRunning(): Boolean = scheduled?.isShutdown ?: false

    override fun start() {
        log.info("Starting the scheduled service")
        scheduled = Executors.newSingleThreadScheduledExecutor()
        scheduled?.scheduleAtFixedRate({ doSomethingUseful() }, 500, 500, TimeUnit.MILLISECONDS)
    }

    override fun stop(callback: Runnable) {
        log.info("Stopping the scheduled service")
        scheduled?.awaitTermination(5, TimeUnit.SECONDS)
        callback.run()
    }

    /**
     * {@link BatchJob#stop(Callback} should be called instead.
     */
    override fun stop() {
        throw IllegalStateException("This method shouldn't be called!")
    }

    private fun doSomethingUseful() {
        try {
            usefulService.doTheThing()
        } catch (e: Exception) {
            // Swallow the exception or the scheduler will stop!
            log.error("Failed to do the thing: ${e.javaClass.simpleName}")
        }
    }
}
