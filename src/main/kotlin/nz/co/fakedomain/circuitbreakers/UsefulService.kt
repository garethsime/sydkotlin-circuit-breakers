package nz.co.fakedomain.circuitbreakers

import io.github.resilience4j.circuitbreaker.CircuitBreaker

interface UsefulService {
    fun doTheThing()
}

/**
 * Does something useful over HTTP.
 */
class ServiceA(
        val serviceB: MyCoolHttpClient,
        val circuitBreaker: CircuitBreaker
): UsefulService {
    override fun doTheThing() {
        val result = serviceB.post()
        println(result)
    }
}
