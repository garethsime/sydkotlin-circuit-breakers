package nz.co.fakedomain.circuitbreakers

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CircuitBreakersApplication

fun main(args: Array<String>) {
    runApplication<CircuitBreakersApplication>(*args)
}
