package nz.co.fakedomain.circuitbreakers

import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.Test

class MyCoolHttpClientTest {
    @Test
    fun `Default client works`() {
        val client = MyCoolHttpClient()
        client.post()
    }

    @Test
    fun `Broken client works`() {
        val client = MyCoolHttpClient()
        client.breakClient()
        assertThatThrownBy { client.post() }.isInstanceOf(RuntimeException::class.java)
    }

    @Test
    fun `Client can be fixed`() {
        val client = MyCoolHttpClient()
        client.breakClient()
        client.fixClient()
        client.post()
    }
}
